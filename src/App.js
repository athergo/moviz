import React, { Component } from 'react';
import './App.css';
import {Header, MovieList, MovieDetails} from './components';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      movies:[{
        title:'Le Parrain',
        img:'https://image.tmdb.org/t/p/w1280/wnDNKCeBQzioXYQrXcSyrmRHVxf.jpg',
        details:'R | 175 min | Crime,Drama',
        description:"En 1945, à New York, les Corleone sont une des cinq familles de la mafia. Don Vito Corleone, 'parrain' de cette famille, marie sa fille à un bookmaker. Sollozzo, ' parrain' de la famille Tattaglia, propose à Don Vito une association dans le trafic de drogue, mais celui-ci refuse. Sonny, un de ses fils, y est quant à lui favorable. Afin de traiter avec Sonny, Sollozzo tente de faire tuer Don Vito, mais celui-ci en réchappe. Michael, le frère cadet de Sonny, recherche alors les commanditaires de l'attentat et tue Sollozzo et le chef de la police, en représailles. Michael part alors en Sicile, où il épouse Apollonia, mais celle-ci est assassinée à sa place. De retour à New York, Michael épouse Kay Adams et se prépare à devenir le successeur de son père..."
      },{
        title:"Le Seigneur des anneaux : La Communauté de l'anneau",
        img:'https://image.tmdb.org/t/p/w1280/5OPg6M0yHr21Ovs1fni2H1xpKuF.jpg',
        details:'R | 165 min | Aventure, Fantastique, Action',
        description:"Le jeune et timide Hobbit, Frodon Sacquet, hérite d'un anneau. Bien loin d'être une simple babiole, il s'agit de l'Anneau Unique, un instrument de pouvoir absolu qui permettrait à Sauron, le Seigneur des ténèbres, de régner sur la Terre du Milieu et de réduire en esclavage ses peuples. À moins que Frodon, aidé d'une Compagnie constituée de Hobbits, d'Hommes, d'un Magicien, d'un Nain, et d'un Elfe, ne parvienne à emporter l'Anneau à travers la Terre du Milieu jusqu'à la Crevasse du Destin, lieu où il a été forgé, et à le détruire pour toujours. Un tel périple signifie s'aventurer très loin en Mordor, les terres du Seigneur des ténèbres, où est rassemblée son armée d'Orques maléfiques... La Compagnie doit non seulement combattre les forces extérieures du mal mais aussi les dissensions internes et l'influence corruptrice qu'exerce l'Anneau lui-même."
      },{
        title:'La Guerre des étoiles',
        img:'https://image.tmdb.org/t/p/w1280/qelTNHrBSYjPvwdzsDBPVsqnNzc.jpg',
        details:'R | 125 min | Aventure, Action, Science-Fiction',
        description:"Il y a bien longtemps, dans une galaxie très lointaine... La guerre civile fait rage entre l'Empire galactique et l'Alliance rebelle. Capturée par les troupes de choc de l'Empereur menées par le sombre et impitoyable Dark Vador, la princesse Leia Organa dissimule les plans de l’Étoile Noire, une station spatiale invulnérable, à son droïde R2-D2 avec pour mission de les remettre au Jedi Obi-Wan Kenobi. Accompagné de son fidèle compagnon, le droïde de protocole C-3PO, R2-D2 s'échoue sur la planète Tatooine et termine sa quête chez le jeune Luke Skywalker. Rêvant de devenir pilote mais confiné aux travaux de la ferme, ce dernier se lance à la recherche de ce mystérieux Obi-Wan Kenobi, devenu ermite au cœur des montagnes désertiques de Tatooine..."
      },{
        title:'12 hommes en colère',
        img:'https://image.tmdb.org/t/p/w1280/bPImGSvZtG2tvsJ9bVLrIECRDnB.jpg',
        details:'R | 95 min | Drama',
        description:"Un jeune homme d'origine modeste est accusé du meurtre de son père et risque la peine de mort. Le jury composé de douze hommes se retire pour délibérer et procède immédiatement à un vote : onze votent coupable, or la décision doit être prise à l'unanimité. Le juré qui a voté non-coupable, sommé de se justifier, explique qu'il a un doute et que la vie d'un homme mérite quelques heures de discussion. Il s'emploie alors à les convaincre un par un."
      },{
        title:'Treize Jours',
        img:'https://image.tmdb.org/t/p/w1280/7TeGocXYo0GJxnAMdw2Kqic4Tw5.jpg',
        details:'R | 145 min | Thriller',
        description:"Le 14 octobre 1962, un avion espion américain découvre la présence de missiles nucléaires soviétiques sur le territoire cubain. S'ils étaient lancés sur les États-Unis, ces missiles pourraient rayer de la carte de nombreuses villes américaines et tuer plus de 80 millions d'habitants. Entre les deux superpuissances, le bras de fer commence. Pour John Fitzgerald Kennedy, le Président des États-Unis, la menace est imminente. À ses côtés se trouvent deux hommes de confiance : Robert, son frère, et Kenneth O'Donnell, le chef d’État-major. Durant 13 jours et dans le secret de la Maison Blanche, les trois hommes seront au centre de la plus incroyable et de la plus dangereuse des négociations."
      },{
        title:'13 Hours',
        img:'https://image.tmdb.org/t/p/w1280/2Thhpd852zt4wFemw95KMNqQPC7.jpg',
        details:'R | 144 min | Action, Histoire, Thriller',
        description:"L'histoire vraie des événements survenus le 11 septembre 2012, lorsque des terroristes ont attaqué un camp des Missions Spéciales de l'Armée Américaine et une agence de la CIA voisine à Benghazi, en Libye. Une attaque repoussée par six opérateurs de sécurité, qui ont lutté pendant 13 heures."
      },{
        title:'La chute du faucon noir',
        img:'https://image.tmdb.org/t/p/w1280/8PbLgvdfcdBad3sie9cfIBHq2Xz.jpg',
        details:'R | 143 min | Action, Histoire, Guerre',        
        description:"Le 3 octobre 1993, avec l'appui des Nations Unies, une centaine de marines américains de la Task Force Ranger est envoyée en mission à Mogadiscio, en Somalie, pour assurer le maintien de la paix et capturer les deux principaux lieutenants et quelques autres associés de Mohamed Farrah Aidid, un chef de guerre local. Cette opération de routine vire rapidement au cauchemar lorsque les militaires sont pris pour cibles par les factions armées rebelles et la population, résolument hostiles à toute présence étrangère sur leur territoire."
      },{
        title:'Il faut sauver le soldat Ryan',
        img:'https://image.tmdb.org/t/p/w1280/mlSsQWIQV0NKIqqRQRI0yi9gqk8.jpg',
        details:'R | 163 min | Drame, Histoire, Guerre',
        description:"Alors que les forces alliées débarquent à Omaha Beach, Miller doit conduire son escouade derrière les lignes ennemies pour une mission particulièrement dangereuse : trouver et ramener sain et sauf le simple soldat James Ryan, dont les trois frères sont morts au combat en l'espace de trois jours. Pendant que l'escouade progresse en territoire ennemi, les hommes de Miller se posent des questions. Faut-il risquer la vie de huit hommes pour en sauver un seul ?"
      }],
      selectedMovie:0,
    }
  }

  render(){
    return (
      <div className="App d-flex flex-column">
       <Header/>
       <div className="d-flex flex-row flex-fill pt-4 p-2">
       <MovieList/>
       <MovieDetails/>
       </div>
      </div>
    );
  }
}



export default App;
